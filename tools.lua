

local tools = {}

function tools.storeJSON(obj, name)
    local recipe_handle = fs.open(name, "w")
    local recipe_raw = recipe_handle.write(textutils.serializeJSON(obj))
    recipe_handle.close()
end

function tools.loadJSON(name)
    local recipe_handle = fs.open(name, "r")
    local recipe_raw = recipe_handle.readAll()
    local obj = textutils.unserializeJSON(recipe_raw)
    recipe_handle.close()
    return obj
end

function tools.printMinecraftItem(name, prefix, suffix)
    prefix = prefix or ""
    suffix = suffix or ""
    if name == nil then
        name = "Empty"
        print(tostring(prefix) .. name .. tostring(suffix))
        return
    end
    local index = string.find(name, ":")
    local mod = string.sub(name, 1, index - 1)
    local item = string.sub(name, index + 1)
    term.write(prefix)
    term.setTextColor(colors.gray)
    term.write(mod)
    term.write(":")
    term.setTextColor(colors.lightGray)
    term.write(item)
    term.setTextColor(colors.white)
    term.write(suffix)
    print()
end

function tools.printRecipe(recipe)
    tools.printMinecraftItem(recipe.output[1], "[Recipe ", "]")
    print("Items to trigger this recipe:")
    for i, item in pairs(recipe.trigger_items) do
        tools.printMinecraftItem(item, "  " .. tostring(i) .. ": ")
    end
    print()
    print("Pedestal items:")
    for i, item in pairs(recipe.input_pedestal) do
        tools.printMinecraftItem(item, "  " .. tostring(i) .. ": ")
    end
    print()
    print("Chamber input: ")
    for i, item in pairs(recipe.input_chamber) do
        tools.printMinecraftItem(item, "  " .. tostring(i) .. ": ")
    end
    print()
end


return tools
