term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
term.clear()
term.setCursorPos(1, 1)
print("Loading Auto...")
-- Pattern Provider Chest
local input_chest = peripheral.wrap("minecraft:chest_2")
local output_chest = peripheral.wrap("minecraft:chest_5")
local recipe_items = peripheral.wrap("minecraft:chest_3")
local learn_recipe = peripheral.wrap("minecraft:barrel_1")

local input_chest_name = peripheral.getName(input_chest)
local output_chest_name = peripheral.getName(output_chest)

local pedestal_1 = peripheral.wrap("ars_nouveau:arcane_pedestal_0")
local pedestal_2 = peripheral.wrap("ars_nouveau:arcane_pedestal_1")
local pedestal_3 = peripheral.wrap("ars_nouveau:arcane_pedestal_2")

local pedestal_1_name = peripheral.getName(pedestal_1)
local pedestal_2_name = peripheral.getName(pedestal_2)
local pedestal_3_name = peripheral.getName(pedestal_3)

local chamber_top = peripheral.wrap("ars_nouveau:imbuement_chamber_0")
local chamber_bottom = peripheral.wrap("ars_nouveau:imbuement_chamber_1")
local chamber_top_name = peripheral.getName(chamber_top)
local chamber_bottom_name = peripheral.getName(chamber_bottom)

local learn = require("learning")
local tools = require("tools")

local w, h = term.getSize()

local recipes = {}
if fs.exists("recipes2.lua") then
    print("Loading recipes...")
    recipes = tools.loadJSON("recipes.json")
    print("Loaded recipes")
else
    print("Adding empty recipe list...")
    tools.storeJSON(recipes, "recipes.json")
    print("Done.")
end

sleep(3)
-- local recipes = require("recipes")
--[[
    Steps:
    Read Input Chest
        Has Items?
            Yes: Continue
            No: Wait 1 second, then try again

    Determine if recipe from input chest exists

    If not, learn new Recipe.
        - Read all [pedestals]
        - Read [chamber]
        - Read [chamber] until it changes
        - Save recipe

    Recipe known:
        - Find recipe
        - Move items from [recipe chest] to [pedestals]
        - Move main item from [input chest] to [chamber] (split between the two)
        - Wait for [chamber] to finish
        - Move item from [chamber] to [output chest] (pattern provider'ish)
        - Move items from [pedestals] to [recipe chest]

]]


local function printName(name)
    local index = string.find(name, ":")
    local mod = name:sub(1, index - 1)
    local name = name:sub(index + 1)

    term.setTextColor(colors.gray)
    term.write(mod .. ":")
    term.setTextColor(colors.lightGray)
    term.write(name)
    term.setTextColor(colors.white)
    print()
end

local function printNameP(prefix, name)
    name = name or "Empty"
    local index = string.find(name, ":")
    if not index then
        term.setTextColor(colors.white)
        term.write(prefix)
        term.setTextColor(colors.gray)
        term.write(name)
        term.setTextColor(colors.lightGray)
        print()
        term.setTextColor(colors.white)
        return
    end
    local mod = name:sub(1, index - 1)
    local name = name:sub(index + 1)

    term.setTextColor(colors.white)
    term.write(prefix)
    term.setTextColor(colors.gray)
    term.write(mod .. ":")
    term.setTextColor(colors.lightGray)
    term.write(name)
    term.setTextColor(colors.white)
    print()
end

local function between(input, min, max)
    return input >= min and input <= max
end

local function move(item, fromPeripheral, toName, toSlot)
    if item == nil or item == "" or fromPeripheral == nil or toName == nil or toSlot == nil then
        return false
    end
    local items = fromPeripheral.list()
    local fromSlot = nil
    for i, t_item in pairs(items) do
        if t_item.name == item then
            fromSlot = i
            break
        end
    end

    if fromSlot == nil then
        -- print(tostring(item) .. " not found")
        return false
    end
    return fromPeripheral.pushItems(toName, fromSlot, toSlot)
end

local function moveFrom(fromPeripheral, toPeripheral)
    if fromPeripheral == nil or toPeripheral == nil then
        return false
    end
    local fromName = peripheral.getName(fromPeripheral)
    local toName = peripheral.getName(toPeripheral)

    local items = fromPeripheral.getItemDetail(1)
    return toPeripheral.pullItems(fromName, 1, 1)
    -- return fromPeripheral.pushItems(toName, 1)
end

local function resetRecipe()
    moveFrom(pedestal_1, recipe_items)
    moveFrom(pedestal_2, recipe_items)
    moveFrom(pedestal_3, recipe_items)
    moveFrom(chamber_top, output_chest)
    moveFrom(chamber_bottom, output_chest)
end

local function setRecipe(recipe)
    move(recipe.input_pedestal[1], recipe_items, pedestal_1_name, 1)
    move(recipe.input_pedestal[2], recipe_items, pedestal_2_name, 1)
    move(recipe.input_pedestal[3], recipe_items, pedestal_3_name, 1)
    sleep(0.1)
    move(recipe.input_chamber[1], input_chest, chamber_top_name, 1)
    move(recipe.input_chamber[1], input_chest, chamber_bottom_name, 1)
end

local function isChamberComplete(chamberPeripheral, expectedName)
    local item = chamberPeripheral.getItemDetail(1)
    if item == nil then
        return true
    end
    if item.name ~= expectedName then
        return false
    end
    return true
end

local function isRecipeComplete(recipe)
    if not isChamberComplete(chamber_top, recipe.output[1]) then
        return false
    end
    if not isChamberComplete(chamber_bottom, recipe.output[1]) then
        return false
    end
    return true
end

local ChamberOutput = nil
local ChamberInput = nil


local function tPrint(...)
    local preX, preY = term.getCursorPos()
    local prev = {x = preX, y = preY}
    term.setCursorPos(1, 1)

    term.write(table.unpack(arg))
    term.write("    ")
    term.setCursorPos(prev.x, prev.y)
end

local function isKnownRecipe(list_of_items)
    for k, recipe in pairs(recipes) do
        local trigger = recipe.trigger_items
        local matches = 0
        for i, item in ipairs(trigger) do
            for j, input_item in ipairs(list_of_items) do
                if item == input_item.name then
                    matches = matches + 1
                    break
                end
            end
        end
        if matches == #trigger then
            return true, recipe
        end
    end
    return false, nil
end

local function loop()
    -- Read Input Chest
    local input_items = input_chest.list()
    if #input_items == 0 then
        return
    end

    -- Determine if recipe from input chest exists
    local known, recipe = isKnownRecipe(input_items)
    if known then
        -- Set Recipe
        setRecipe(recipe)
        -- Wait for Recipe to finish
        while not isRecipeComplete(recipe) do
            sleep(0.25)
        end
        -- Move Result to Output Chest
        moveFrom(chamber_top, output_chest)
        moveFrom(chamber_bottom, output_chest)

        -- Move all trigger items to output chest
        for i, item in ipairs(recipe.trigger_items) do
            move(item, input_chest, output_chest_name, 1)
        end

        resetRecipe()
        return
    else
        local tok, trecipe = learn({
            input_chest = input_chest,
            recipe_chest = recipe_items,
            output_chest = output_chest,
            chamber_top = chamber_top,
            pedestal_1 = pedestal_1,
            pedestal_2 = pedestal_2,
            pedestal_3 = pedestal_3
        })
        if tok then
            tools.printRecipe(trecipe)

            print("Adding recipe to recipes")
            table.insert(recipes, trecipe)
            tools.storeJSON(recipes, "recipes.json")
            print("Added to recipes")
        end
    end
end


resetRecipe()

while true do
    local timer = os.startTimer(1)
    local eventData = {os.pullEvent()}
    local event = eventData[1]

    if event == "key" or event == "key_up" or event == "char" then
        tPrint("Key code", eventData[2], "was pressed")
        if eventData[2] == 81 then
            break
        end
    elseif event == "timer" then
        loop()
    else
        tPrint("Unknown event " .. event)
    end

end


print("Ended")



