
--[[

Steps for learning:

    - Detected no recipe
    - Save the items in the input chest
    - Wait for chamber top to contain item.
        - Contains Item? No: Wait
            - If input chest is empty, ignore and return to normal operation
        - Contains Item? Yes:
            - Save starting item in chamber top
            - Wait for item to be different
            - Save ending item in chamber top
            - Save items from pedestals (if any)
            - Save entire recipe from previous data
            - Confirm learned recipe by moving chamber final item to output chest
            - Move recipe items to recipe chest

]]

---@ function learn
---@ param peripherals: table {
---@    input_chest: peripheral
---@    recipe_chest: peripheral
---@    output_chest: peripheral
---@    chamber_top: peripheral
---@    pedestal_1: peripheral
---@    pedestal_2: peripheral
---@    pedestal_3: peripheral
---@ }
---@
---@ return boolean [, table]
local function learn(peripherals)
    local input_chest = peripherals.input_chest
    local recipe_chest = peripherals.recipe_chest
    local output_chest = peripherals.output_chest
    local chamber_top = peripherals.chamber_top
    local pedestal_1 = peripherals.pedestal_1
    local pedestal_2 = peripherals.pedestal_2
    local pedestal_3 = peripherals.pedestal_3

    if not input_chest then
        error("No input chest peripheral provided")
    end
    if not recipe_chest then
        error("No recipe chest peripheral provided")
    end
    if not output_chest then
        error("No output chest peripheral provided")
    end
    if not chamber_top then
        error("No chamber top peripheral provided")
    end
    if not pedestal_1 then
        error("No pedestal 1 peripheral provided")
    end
    if not pedestal_2 then
        error("No pedestal 2 peripheral provided")
    end
    if not pedestal_3 then
        error("No pedestal 3 peripheral provided")
    end

    print("No recipe matched. Entered learning mode.")
    --
    print("Step-by-step guide for teaching a recipe:")
    print("1. Create AE2 recipe pattern")
    print("2. Place the pattern in the pattern provider")
    print("3. Start a SINGLE autocraft sequence of the pattern")
    print("4. Move the required recipe items from the recipe chest to the pedestals")
    print("5. Move the input item (e.g., source gem) to the top chamber")
    print()
    
    sleep(1)
    print("Reading input chest as trigger items...")
    local pre_items_in_input_chest = input_chest.list()
    print("Trigger items: ")
    for i, item in pairs(pre_items_in_input_chest) do
        print("  " .. tostring(item.name))
    end

    print("Waiting for item in chamber top...")
    while true do
        local chamber_detail = chamber_top.getItemDetail(1)
        if chamber_detail == nil then
            -- print("No item in chamber top. Waiting...")
            sleep(0.1)
        else
            print("Item in chamber top. Waiting for item to change...")
            local starting_chamber_item = chamber_detail
            local resulting_chamber_item = nil
            while true do
                local current_chamber_detail = chamber_top.getItemDetail(1)
                if current_chamber_detail == nil then
                    print("Item in chamber top disappeared. Halting...")
                    sleep(1)
                    return false
                end

                if current_chamber_detail.name == starting_chamber_item.name then
                    print("Waiting... ")
                    sleep(2)
                else
                    -- Chamber item is now different. Take this as the final event to save everything
                    resulting_chamber_item = current_chamber_detail
                    print("Item in chamber top changed. Saving recipe...")

                    local rinput_pedestal = {}
                    local p1 = pedestal_1.getItemDetail(1)
                    local p2 = pedestal_2.getItemDetail(1)
                    local p3 = pedestal_3.getItemDetail(1)
                    if p1 ~= nil then
                        table.insert(rinput_pedestal, p1.name)
                    end
                    if p2 ~= nil then
                        table.insert(rinput_pedestal, p2.name)
                    end
                    if p3 ~= nil then
                        table.insert(rinput_pedestal, p3.name)
                    end
                    local recipe = {
                        trigger_items = {},
                        input_chamber = {
                            starting_chamber_item.name
                        },
                        output = {
                            resulting_chamber_item.name
                        },
                        input_pedestal = rinput_pedestal
                    }
                    for i, item in pairs(pre_items_in_input_chest) do
                        table.insert(recipe.trigger_items, item.name)
                    end
                    
                    local recipe_chest_name = peripheral.getName(recipe_chest)
                    local output_chest_name = peripheral.getName(output_chest)
                    chamber_top.pushItems(output_chest_name, 1)
                    pedestal_1.pushItems(recipe_chest_name, 1)
                    pedestal_2.pushItems(recipe_chest_name, 1)
                    pedestal_3.pushItems(recipe_chest_name, 1)
                    
                    -- List input chest items and forward to output chest
                    for i, item in pairs(pre_items_in_input_chest) do
                        input_chest.pushItems(output_chest_name, i)
                    end

                    return true, recipe
                end
            end
        end
    end
end


return learn